# README #

This applications requires node.js and vue 2 cli.
Uses vue framework - vutify 2

### To install ###

* clone/download to your system
* run npm install

### To edit ###

* run npm run dev
This will build the app and launch a website that stay up to date with hot reload.


### To build ###

* run npm run build
This will build the app and place the compiled app in the dist folder, reay to be uploaded to your hosting.
